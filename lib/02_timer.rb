class Timer
  attr_accessor :seconds

  def initialize(seconds = 0)
    @seconds = seconds
  end

  def string_setup(int)
    if int > 10
      "#{int}"
    else
      "0#{int}"
    end
  end

  def second
    second = (seconds % 60)
  end

  def minutes
    minute = ((seconds % 3600) / 60)
  end

  def hours
    hour = (seconds / 3600)
  end

  def time_string
    "#{string_setup(hours)}:#{string_setup(minutes)}:#{string_setup(second)}"
  end
  
end
