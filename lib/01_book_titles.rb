class Book

  WORDS_NVR_CAP = ["a", "the", "an", "in", "of", "and"]

  attr_reader :title


  def title=(title)
    original_words = title.split(" ").map(&:downcase)

    title_words = original_words.map.with_index do |word, idx|
      if WORDS_NVR_CAP.include?(word) && idx != 0
        word
      elsif word.length == 1 && word[0] == "i"
        word.capitalize
      else
        word.capitalize
      end
    end

    @title = title_words.join(" ")


  end
end
