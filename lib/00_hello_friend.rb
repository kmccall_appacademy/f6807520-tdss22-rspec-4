class Friend
  def initialize(name = "Bob")
    @name = name
  end

  def greeting(name = false)
    if name
      "Hello, #{@name}!"
    else
      "Hello!"
    end
  end
end
