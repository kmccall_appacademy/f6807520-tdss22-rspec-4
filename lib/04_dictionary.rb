class Dictionary
  attr_reader :entries

  def initialize
    @entries = {}
  end

  def add(entry)
    if entry.is_a?(Hash)
      @entries.merge!(entry)
    elsif entry.is_a?(String)
      @entries[entry] = nil
    end
  end

  def find(sub)
    @entries.select do |word, define|
      word.match(sub)
    end
  end
  def keywords
    @entries.keys.sort { |k, v| k <=> v }
  end

  def include?(word)
    @entries.keys.include?(word)
  end

  def printable
    entries = keywords.map do |keyword|
    %Q{[#{keyword}] "#{@entries[keyword]}"}
    end

    entries.join("\n")


  end
end
